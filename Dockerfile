FROM tomcat:9-jdk21-openjdk-slim-bullseye
LABEL "MAINTAINER" = "harishahk14@gmail.com"
WORKDIR /usr/local/tomcat
RUN mv webapps.dist/* webapps/
COPY webapp/target/*.war webapps/
EXPOSE 8080
RUN adduser --system --shell /bin/false --no-create-home tomcat
USER tomcat
CMD [ "catalina.sh", "run" ]