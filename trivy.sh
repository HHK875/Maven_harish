#!/bin/sh
################################
# Author: HHK
# Email-ID: hhk14@gmail.com
# Version: 1.0
################################
mkdir -p $HOME/Library/Caches
docker run -v /var/run/docker.sock:/var/run/docker.sock -v $HOME/Library/Caches:/root/.cache/ aquasec/trivy:0.45.0 image   myapp:v1.0
